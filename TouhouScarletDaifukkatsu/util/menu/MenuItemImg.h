#pragma once
#include <SFML/graphics.hpp>
#include "MenuItem.h"

class MenuItemImg : public MenuItem
{
public:
	MenuItemImg(sf::Sprite& img, sf::Sprite& imgF);

	virtual void yield();
	virtual void initHitbox();
	virtual void draw(bool selected, sf::RenderTarget& target);

protected:
	sf::Sprite img;
	sf::Sprite imgF;
private:
};
