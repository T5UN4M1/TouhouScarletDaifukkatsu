#include "MenuItemImg.h"

MenuItemImg::MenuItemImg(sf::Sprite& img, sf::Sprite& imgF) {
	this->img = sf::Sprite(img);
	this->imgF = sf::Sprite(imgF);
	initHitbox();
}

void MenuItemImg::yield() {
	//ProjetTerrariaView::rt.draw(selected ? imgF : img);
}

void MenuItemImg::initHitbox() {
	this->hitbox = this->img.getGlobalBounds();
}

void MenuItemImg::draw(bool selected, sf::RenderTarget & target)
{
	target.draw(selected ? imgF : img);
}
