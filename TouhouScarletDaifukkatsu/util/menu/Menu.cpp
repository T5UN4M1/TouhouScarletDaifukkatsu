#include "Menu.h"
#include "MenuItemText.h"

Menu::Menu() {
	this->reset();
}
void Menu::reset() {
	items.clear();
	select = 0;
}

void Menu::addItem(MenuItem* item) {
	items.push_back(std::unique_ptr<MenuItem>(item));
}
void Menu::operator++() {
	setSelect(select + 1);
	isMouseActive = false;
}
void Menu::operator--() {
	setSelect(select - 1);
	isMouseActive = false;
}
void Menu::setSelect(int select) {
	if (select >= static_cast<int>(items.size())) {
		select = 0;
	}
	else if (select<0) {
		select = items.size() - 1;
	}
	this->select = select;
}
int Menu::getSelect()const {
	return select;
}
void Menu::yield() {
	if (!isMouseActive && sf::Mouse::getPosition() != this->mousePos) {
		this->isMouseActive = true;
	}

	this->mousePos = sf::Mouse::getPosition();

	for (int i = 0; i<static_cast<int>(items.size()); ++i) {
		if (isMouseActive && select != i && items[i]->getHitbox().contains(mousePos.x, mousePos.y)) {
			this->setSelect(i);
		}
		items[i]->yield();
	}
}

std::vector<std::unique_ptr<MenuItem>>& Menu::getItems() {
	return items;
}

void Menu::mkMenu(std::vector<std::pair<sf::Vector2f, std::string>>& elements, sf::Text& defaultIdle, sf::Text& defaultFocused) {
	for (unsigned i = 0; i<elements.size(); ++i) {
		sf::Text t1(defaultIdle);
		sf::Text t2(defaultFocused);
		t1.setPosition(elements[i].first);
		t2.setPosition(elements[i].first);
		t1.setString(elements[i].second);
		t2.setString(elements[i].second);
		addItem(new MenuItemText(t1, t2));
	}
}
