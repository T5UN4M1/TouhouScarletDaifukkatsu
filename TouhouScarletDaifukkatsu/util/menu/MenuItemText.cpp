#include "MenuItemText.h"

MenuItemText::MenuItemText(sf::Text& text, sf::Text& textF) {
	this->text = sf::Text(text);
	this->textF = sf::Text(textF);
	initHitbox();
}

void MenuItemText::yield() {
	//ProjetTerrariaView::rt.draw(selected ? textF : text);
}

void MenuItemText::initHitbox() {
	this->hitbox = this->text.getGlobalBounds();
}
void MenuItemText::draw(bool selected, sf::RenderTarget & target)
{
	target.draw(selected ? textF : text);
}