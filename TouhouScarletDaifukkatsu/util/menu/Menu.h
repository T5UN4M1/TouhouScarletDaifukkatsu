#pragma once

#include <vector>
#include <memory>

#include "MenuItem.h"
class Menu
{
public:
	Menu();

	Menu(const Menu& menu) = delete;
	Menu& operator=(const Menu& other) = delete;

	void reset();

	void addItem(MenuItem* item);

	void operator++();
	void operator--();

	void setSelect(int select);
	int getSelect()const;

	void mkMenu(std::vector<std::pair<sf::Vector2f, std::string>>& elements, sf::Text& defaultIdle, sf::Text& defaultFocused);

	void yield();

	std::vector<std::unique_ptr<MenuItem>>& getItems();

protected:

private:
	sf::Vector2i mousePos;
	bool isMouseActive;
	std::vector<std::unique_ptr<MenuItem>> items;
	int select;


};
