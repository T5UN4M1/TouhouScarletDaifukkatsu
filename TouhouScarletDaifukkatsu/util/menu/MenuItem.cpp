#include "MenuItem.h"

MenuItem::MenuItem() {
	focusFrame = 0;
	activationFrame = 0;
}

void MenuItem::setHitbox(sf::FloatRect& hitbox)
{
	this->hitbox = hitbox;
}
const sf::FloatRect& MenuItem::getHitbox()const {
	return hitbox;
}

void MenuItem::giveFocus()
{
	focusFrame = 1;
}

void MenuItem::removeFocus()
{
	focusFrame = 0;
}

void MenuItem::onActivation()
{
	activationFrame = 1;
}
