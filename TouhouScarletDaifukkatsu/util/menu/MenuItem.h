#pragma once

#include <SFML/graphics.hpp>

class MenuItem
{
public:
	MenuItem();

	void setHitbox(sf::FloatRect& hitbox); // force the hitbox
	const sf::FloatRect& getHitbox()const; // return current hitbox

	virtual void yield() = 0;
	virtual void initHitbox() = 0;
	virtual void draw(bool selected, sf::RenderTarget& target) = 0;

	virtual void giveFocus();
	virtual void removeFocus();
	virtual void onActivation();


protected:
	sf::FloatRect hitbox;
	int focusFrame; // 0 -> no focus , >0 -> focus
	int activationFrame; // same as focus

private:
};
