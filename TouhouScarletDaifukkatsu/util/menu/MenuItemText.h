#pragma once
#include <SFML/graphics.hpp>
#include "MenuItem.h"

class MenuItemText : public MenuItem
{
public:
	MenuItemText(sf::Text& text, sf::Text& textF);

	virtual void yield();
	virtual void initHitbox();
	virtual void draw(bool selected, sf::RenderTarget& target);

protected:
	sf::Text text;
	sf::Text textF;
private:
};
