#include "button.h"

Button::Button(){
    framePushed = 0;
}

bool Button::isPushed() const{
    return framePushed > 0;
}

bool Button::isActivated(int delay, int interval) const{
    return (framePushed == 1 || (framePushed >= delay && framePushed%interval==0));
}

int Button::getFrames() const{
    return framePushed;
}

void Button::yield(){
}
