#ifndef INPUTACTION_H
#define INPUTACTION_H


class InputAction
{
    public:
        virtual bool isPushed()const=0;
        virtual bool isActivated(int delay,int interval)const=0;
        virtual int getFrames()const=0;
        virtual void yield()=0;
};

#endif // INPUTACTION_H
