#ifndef KEY_H
#define KEY_H

#include "button.h"
#include <SFML/Graphics.hpp>
class Key : public Button
{
    public:
        Key(sf::Keyboard::Key key);
        virtual void yield();
    protected:
        sf::Keyboard::Key key;
};

#endif // KEY_H
