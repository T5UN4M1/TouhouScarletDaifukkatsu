#ifndef INPUT_H
#define INPUT_H

#include <vector>
#include <unordered_map>
#include <memory>
#include "button.h"
#include "InputAction.h"

class Input
{
    public:
        Input();
        void reset();
        void yield();
        void addButton(std::string code, InputAction* button);
        bool isPressed(std::string code);
        bool isActivated(std::string code, int delay=30, int interval=5);
		InputAction* get(std::string code);
    protected:
        //std::vector<std::unique_ptr<InputAction>> keySet;
		std::unordered_map<std::string, std::unique_ptr<InputAction>> keySet;
};

#endif // INPUT_H
