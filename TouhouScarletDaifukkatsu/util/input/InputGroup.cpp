#include "InputGroup.h"

InputGroup::InputGroup(std::vector<InputAction*> inputs){
    for(InputAction* el : inputs){
        keySet.push_back(std::unique_ptr<InputAction>(el));
    }
}
bool InputGroup::isPushed()const{
    for(int i=0;i<keySet.size();++i){
        if(keySet[i]->isPushed()){
            return true;
        }
    }
    return false;
}
bool InputGroup::isActivated(int delay,int interval)const{
    for(int i=0;i<keySet.size();++i){
        if(keySet[i]->isActivated(delay,interval)){
            return true;
        }
    }
    return false;
}
int InputGroup::getFrames()const{
    if(isPushed()){
        return 1;
    }
    return 0;
}
void InputGroup::yield(){
    for(int i=0;i<keySet.size();++i){
        keySet[i]->yield();
    }
}
