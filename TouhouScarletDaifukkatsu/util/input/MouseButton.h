#ifndef MOUSEBUTTON_H
#define MOUSEBUTTON_H

#include <SFML/Graphics.hpp>
#include "button.h"

class MouseButton : public Button
{
    public:
        MouseButton(sf::Mouse::Button button);
        virtual void yield();
    protected:
        sf::Mouse::Button button;
};

#endif // MOUSEBUTTON_H
