#include "input.h"

Input::Input(){
}

void Input::reset(){
    keySet.clear();
}

void Input::yield(){
	for (auto& inputAction : keySet) {
		inputAction.second->yield();
	}
}

void Input::addButton(std::string code,InputAction* button){
	keySet[code] = std::unique_ptr<InputAction>(button);
}

bool Input::isPressed(std::string code){
    return keySet[code]->isPushed();
}

bool Input::isActivated(std::string code, int delay, int interval){
    return keySet[code]->isActivated(delay,interval);
}
InputAction* Input::get(std::string code){
	return keySet[code].get();
}