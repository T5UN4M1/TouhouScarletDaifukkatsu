#ifndef INPUTGROUP_H
#define INPUTGROUP_H

#include <vector>
#include <memory>
#include "InputAction.h"
class InputGroup : public InputAction
{
    public:
        InputGroup(std::vector<InputAction*> inputs);

        virtual bool isPushed()const;
        virtual bool isActivated(int delay,int interval)const;
        virtual int getFrames()const;
        virtual void yield();
    protected:
        std::vector<std::unique_ptr<InputAction>> keySet;

};

#endif // INPUTGROUP_H
