#include "mousebutton.h"

MouseButton::MouseButton(sf::Mouse::Button button){
    this->button = button;
}
void MouseButton::yield(){
    if(sf::Mouse::isButtonPressed(button)){
        ++framePushed;
    } else {
        framePushed = 0;
    }
}
