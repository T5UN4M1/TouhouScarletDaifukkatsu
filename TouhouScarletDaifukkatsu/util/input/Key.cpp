#include "key.h"

Key::Key(sf::Keyboard::Key key):Button(){
    this->key = key;
}
void Key::yield(){
    if(sf::Keyboard::isKeyPressed(key)){
        ++framePushed;
    } else {
        framePushed = 0;
    }
}
