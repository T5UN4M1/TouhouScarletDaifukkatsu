#pragma once
#include <SFML/graphics.hpp>

#include <string>
#include <sstream>

#include <iostream>

class Tools
{
public:

};

void makeRectangle(sf::VertexArray& array, unsigned rectangleId, sf::FloatRect pos); // makes a rectangle in a vertexarray
void makeRectangle(sf::VertexArray& array, unsigned rectangleId, sf::FloatRect pos,sf::Color color); // same but also gives that rectangle a color

template<class T>
std::string toStr(T nb) {
	std::stringstream ss;
	ss << nb;
	return ss.str();
}

