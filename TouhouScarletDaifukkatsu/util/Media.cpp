#include "Media.h"
#include "LoadScreen.h"
#include <thread>

#include "MenuScreen.h"

#define ROOT_PATH "./"

#define ASSETS_PATH ROOT_PATH "assets/"

#define GUI_PATH ASSETS_PATH "gui/"
#define FONTS_PATH ASSETS_PATH "fonts/"

std::unordered_map<std::string, sf::Texture> Media::t;

bool Media::loading;



sf::Sprite Media::selectBg;
sf::Sprite Media::loadScreenBg;


sf::Sprite Media::cursor;

std::vector<sf::Font> Media::fonts;

sf::Text Media::debugInformations;

MediaLoadingState Media::state = NOTHING_LOADED;



void Media::loadMinimal() {
	t["loadScreenBg"].loadFromFile(GUI_PATH"loadScreenBg.jpg");
	for (unsigned i = 0; i < 4; ++i) {
		fonts.push_back(sf::Font());
	}
	fonts[0].loadFromFile(FONTS_PATH"Arial.ttf");
	debugInformations.setFont(fonts[0]);
	debugInformations.setFillColor(sf::Color::White);
	debugInformations.setCharacterSize(14);
	arrangeSprite(0);
}
void Media::loadMenu() {
	t["menuBg"].loadFromFile(GUI_PATH"menuBg.jpg"); LoadScreen::addProgression(450000);
	t["selectBg"].loadFromFile(GUI_PATH"selectBg.jpg"); LoadScreen::addProgression(450000);
	t["cursor"].loadFromFile(GUI_PATH"cursor.png");



	
	fonts[1].loadFromFile(FONTS_PATH"Gunship.ttf");
	fonts[2].loadFromFile(FONTS_PATH"OldLondon.ttf");
	fonts[3].loadFromFile(FONTS_PATH"SomethingStrange.ttf"); LoadScreen::addProgression(100000);

	

	arrangeSprite(1);
}
void Media::loadGame() {

}
void Media::arrangeSprite(int id) {
	switch (id) {
	case 0:
		loadScreenBg.setTexture(t["loadScreenBg"]);
		state = MINIMAL_LOADED;
		loading = false;
		break;
	case 1:
		MenuScreen::menuBg.setTexture(t["menuBg"]); 
		selectBg.setTexture(t["selectBg"]); 
		cursor.setTexture(t["cursor"]);
		state = MENU_LOADED;
		loading = false;
		break;
	}

}
void Media::load(void(*f)()) {
	//LoadScreen::progression = 0;
	LoadScreen::resetProgression();
	loading = true;

	std::thread t(f);
	t.detach();
}