#include "Core.h"
#include "Media.h"
#include "LoadScreen.h"
#include "MenuScreen.h"

std::unique_ptr<sf::RenderWindow> Core::window;

sf::RenderTexture Core::rtLeftPanel;
sf::RenderTexture Core::rtGamePanel;
sf::RenderTexture Core::rtRightPanel;

sf::RenderTexture Core::rtGlobalPanel;
sf::RenderTexture Core::rtPause;

sf::Sprite Core::spLeftPanel;
sf::Sprite Core::spGamePanel;
sf::Sprite Core::spRightPanel;

sf::Sprite Core::spGlobalPanel;
sf::Sprite Core::spPause;

sf::Vector2f Core::cursorPos;
bool Core::cursorVisible;

int Core::frame = 0;
CoreState Core::state = CoreState::STARTED;

void Core::run()
{
	init();

	while (window->isOpen()) {
		sf::Event event;
		while (window->pollEvent(event)) {
			if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
				window->close();
			}
		}

		window->clear();

		yield();

		window->display();
	}
}

void Core::yield()
{
	
	switch (state) {
	case STARTED:
		if (frame == 0) {
			Media::load(Media::loadMinimal);
		}
		if (!Media::loading) {
			setState(CoreState::LOADING_FOR_MENU);
		}
		break;
	case LOADING_FOR_MENU:
		if (frame == 0) {
			Media::load(Media::loadMenu);
		}
		if (!Media::loading) {
			setState(CoreState::MENU);
		}
		LoadScreen::yield();
		writeDebugInformations();
		buildGlobal();
		drawGlobal();
		//window->draw(Media::loadingScreenBg);
		break;
	case MENU:
		MenuScreen::yield();
		writeDebugInformations();
		buildGlobal();
		drawGlobal();
		break;

	}
	++frame;
	
}

void Core::init()
{
	// setting up the window
	window = std::make_unique<sf::RenderWindow>(sf::VideoMode::getDesktopMode(), "Touhou Scarlet Daifukkatsu", sf::Style::Fullscreen);
	window->setVerticalSyncEnabled(true);
	window->setMouseCursorVisible(false);
	window->setMouseCursorGrabbed(true);
	setGlobalRtScale();

	// setting up renderTextures
	rtLeftPanel.create(480, 1080);
	rtRightPanel.create(480, 1080);
	rtGamePanel.create(960, 1080);
	rtGlobalPanel.create(1920, 1080);
	rtPause.create(1920, 1080);

	// and their sprites
	spGlobalPanel.setPosition(0, 0);
	spLeftPanel.setPosition(0, 0);
	spRightPanel.setPosition(480 + 960, 0);
	spGamePanel.setPosition(480, 0);
	spPause.setPosition(0, 0);

	rtGlobalPanel.setSmooth(true);
}

void Core::setState(CoreState newState)
{
	state = newState;
	frame = 0;
	yield();
}

void Core::setGlobalRtScale()
{
	spGlobalPanel.setScale(
		window->getSize().x / 1920.0,
		window->getSize().y / 1080.0);
}

void Core::buildGlobal()
{
	rtGlobalPanel.display();
	spGlobalPanel.setTexture(rtGlobalPanel.getTexture());
}

void Core::drawGlobal()
{
	window->draw(spGlobalPanel);
	rtGlobalPanel.clear();
}

void Core::yieldCursor()
{

}

void Core::writeDebugInformations()
{
	std::string displayedState = "";
	switch (state) {
		case CoreState::STARTED:
			displayedState = "STARTED";
			break;
		case CoreState::LOADING_FOR_MENU:
			displayedState = "L4MENU";
			break;
		case CoreState::MENU:
			displayedState = "MENU";
			break;
		case CoreState::LOADING_FOR_GAME:
			displayedState = "L4GAME";
			break;
		case CoreState::GAME:
			displayedState = "GAME";
			break;
	}
	Media::debugInformations.setString("CORE : {S->"+displayedState+" F :"+toStr(frame)+"}");
	Media::debugInformations.setPosition(20, 20);
	Core::rtGlobalPanel.draw(Media::debugInformations);
}
