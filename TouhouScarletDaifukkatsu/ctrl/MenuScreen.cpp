#include "MenuScreen.h"
#include "Core.h"
#include <vector>

sf::Sprite MenuScreen::menuBg;

MenuState MenuScreen::state = MenuState::EMPTY;
Menu MenuScreen::menu;

int MenuScreen::frame = 0;

using namespace std;

void MenuScreen::yield()
{
	Core::rtGlobalPanel.draw(MenuScreen::menuBg);
	if (state == MenuState::EMPTY) {
		setState(MenuState::MAIN_MENU);
	}

}

void MenuScreen::setState(MenuState newState)
{
	state = newState;
	frame = 0;
	/*
	switch (state) {
	case MAIN_MENU:
		vector<pair<sf::Vector2f,string>>  elements;
		elements.push_back(make_pair(sf::Vector2f(300, 300), "Play"));
		menu.mkMenu();
		break;
	case CHAR_SELECT:
		break;
	case OPTIONS:
		break;
	}*/


	yield();
}
