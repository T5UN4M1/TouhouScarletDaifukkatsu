#pragma once

#include <SFML/Graphics.hpp>
#include <memory>
#include "Tools.h"

enum CoreState {
	STARTED,
	LOADING_FOR_MENU,
	MENU,
	LOADING_FOR_GAME,
	GAME
};

class Core
{
public:
	static std::unique_ptr<sf::RenderWindow> window;

	static sf::RenderTexture rtLeftPanel; // 
	static sf::RenderTexture rtGamePanel; // rendering targets for their respective areas
	static sf::RenderTexture rtRightPanel;// 

	static sf::RenderTexture rtGlobalPanel; // this rt is used as a drawing target for everything (everything is drawn to this in menus, the 3 panels are drawn to this while in game) , the sp linked to this rt is then rescaled to fit the window
	static sf::RenderTexture rtPause; // cache rt for pause (global panel is stored inside of this when the game is paused, this rt is then drawn as a background for global panel (and the global panel gets the pause menu)

	static sf::Sprite spLeftPanel;
	static sf::Sprite spGamePanel;
	static sf::Sprite spRightPanel;

	static sf::Sprite spGlobalPanel;
	static sf::Sprite spPause;

	static int frame;  // frame counter 
	static CoreState state;

	static sf::Vector2f cursorPos;
	static bool cursorVisible;

	static void run(); // called at the start of the program
	static void yield(); // called in a loop from run() , main decider that calls the correct class according to the current state
	static void init(); // called at the start

	static void setState(CoreState newState); // method to be used to set the state

	static void setGlobalRtScale(); // sets the scale of the main renderTexture, to be used whenever the window size changes

	static void buildGlobal();
	static void drawGlobal();

	static void yieldCursor();

	static void writeDebugInformations();
};
