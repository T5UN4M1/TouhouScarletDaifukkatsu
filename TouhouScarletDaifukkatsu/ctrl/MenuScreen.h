#pragma once

#include <SFML/Graphics.hpp>
#include "Menu.h"

enum MenuState {
	EMPTY,
	MAIN_MENU,
	CHAR_SELECT,
	OPTIONS
};

class MenuScreen
{
public:


	static sf::Sprite menuBg;
	static MenuState state;
	static Menu menu;

	static int frame;

	static void yield();
	static void setState(MenuState newState);
};

